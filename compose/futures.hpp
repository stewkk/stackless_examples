#pragma once

#include <exe/futures/make/contract.hpp>

#include <coroutine>
#include <optional>

//////////////////////////////////////////////////////////////////////

// Promise Types

namespace exe::futures {

template <typename T>
struct CoroutinePromise {
  std::optional<exe::futures::Promise<T>> promise_;

  auto get_return_object() {
    auto [f, p] = exe::futures::Contract<T>();
    promise_.emplace(std::move(p));
    return std::move(f);
  }

  void return_value(T value) {
    std::move(*promise_).SetValue(std::move(value));
  }

  void set_exception(std::exception_ptr e) {
    std::move(*promise_).SetError(std::move(e));
  }

  void unhandled_exception() {
    std::move(*promise_).SetError(std::current_exception());
  }

  std::suspend_never initial_suspend() noexcept {
    return {};
  }

  std::suspend_never final_suspend() noexcept {
    return {};
  }
};

}  // namespace exe::futures

template <typename R, typename... Args>
struct std::coroutine_traits<exe::futures::Future<R>, Args...> {
  using promise_type = exe::futures::CoroutinePromise<R>;
};

//////////////////////////////////////////////////////////////////////

// Awaiter type

namespace exe::futures {

template <typename T>
class FutureAwaiter {
 public:
  FutureAwaiter(exe::futures::Future<T> f) : future_(std::move(f)) {
  }

  bool await_ready() {
    return false;
  }

  void await_suspend(std::coroutine_handle<> h) {
    std::move(future_).Consume([this, h](exe::Result<T> result) mutable {
      result_.emplace(std::move(result));
      h.resume();
    });
  }

  auto await_resume() {
    return *result_;
  }

 private:
  exe::futures::Future<T> future_;
  std::optional<exe::Result<T>> result_;
};

template <typename T>
auto operator co_await(exe::futures::Future<T> f) {
  return FutureAwaiter<T>(std::move(f));
}

}  // namespace exe::futures

#include <iostream>
#include <chrono>
#include <thread>

#include <coroutine>

#include <exe/executors/thread_pool.hpp>
#include <exe/executors/submit.hpp>

#include "../promises/std_future.hpp"

using namespace exe::executors;

using namespace std::chrono_literals;

//////////////////////////////////////////////////////////////////////

struct TeleportAwaiter {
  ThreadPool& pool;

  bool await_ready() {
    return false;
  }

  void await_suspend(std::coroutine_handle<> h) {
    Submit(pool, [h]() mutable {
      h.resume();
    });
  }

  void await_resume() {
  }
};

auto TeleportTo(ThreadPool& pool) {
  return TeleportAwaiter{pool};
}

//////////////////////////////////////////////////////////////////////

std::future<int> Coro(ThreadPool& pool) {
  std::cout << "Running on thread "
            << std::this_thread::get_id() << std::endl;

  co_await TeleportTo(pool);

  std::cout << "Running on thread "
            << std::this_thread::get_id() << std::endl;

  co_return 7;  // Coroutine!
}

int main() {
  ThreadPool pool{4};
  pool.Start();

  auto f = Coro(pool);
  std::cout << f.get() << std::endl;

  pool.WaitIdle();
  pool.Stop();

  return 0;
}

#pragma once

#include <coroutine>

struct VoidPromise {
  void get_return_object() {
    return;  // Nop
  }

  std::suspend_never initial_suspend() noexcept {
    return {};
  }

  std::suspend_never final_suspend() noexcept {
    return {};
  }

  void set_exception(std::exception_ptr /*e*/) {
    std::abort();  // Not supported
  }

  void unhandled_exception() {
    std::abort();  // Not supported
  }

  void return_void() {
    // Nop
  }
};

template <typename ... Args>
struct std::coroutine_traits<void, Args...> {
  using promise_type = VoidPromise;
};

include(FetchContent)

# --------------------------------------------------------------------

# Offline mode (uncomment next line to enable)
# set(FETCHCONTENT_FULLY_DISCONNECTED ON)

# set(FETCHCONTENT_QUIET OFF)

# --------------------------------------------------------------------

# Libraries

# --------------------------------------------------------------------

message(STATUS "FetchContent: exe")

FetchContent_Declare(
        exe
        GIT_REPOSITORY https://gitlab.com/Lipovsky/exe.git
        GIT_TAG master
)
FetchContent_MakeAvailable(exe)

# --------------------------------------------------------------------

message(STATUS "FetchContent: flow")

FetchContent_Declare(
        flow
        GIT_REPOSITORY https://gitlab.com/Lipovsky/sure-flow.git
        GIT_TAG master
)
FetchContent_MakeAvailable(flow)

# --------------------------------------------------------------------

message(STATUS "FetchContent: asio")

FetchContent_Declare(
        asio
        GIT_REPOSITORY https://github.com/chriskohlhoff/asio.git
        GIT_TAG asio-1-22-1
)
FetchContent_MakeAvailable(asio)

add_library(asio INTERFACE)
target_include_directories(asio INTERFACE ${asio_SOURCE_DIR}/asio/include)
